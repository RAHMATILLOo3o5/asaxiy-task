<?php

namespace app\modules\admin\controllers;

use yii\web\Controller;

/* This class extends the Controller class and has one method called actionIndex() which returns a
string. */
class SiteController extends Controller
{
    /**
     * Renders the index view for the module
     * @return string
     */
    public function actionIndex()
    {

        return $this->render('index');
    }

    public function actionLogin()
    {

        $model = new \app\models\LoginForm();
        if ($model->load(\Yii::$app->request->post()) && $model->login()) {
            return $this->goBack();
        }
        $this->layout = 'main-login';
        return $this->render('login', compact('model'));
    }
}
