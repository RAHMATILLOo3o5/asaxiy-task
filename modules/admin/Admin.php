<?php

namespace app\modules\admin;

use Yii;

/**
 * admin module definition class
 */
class Admin extends \yii\base\Module
{
    /**
     * {@inheritdoc}
     */
    public $controllerNamespace = 'app\modules\admin\controllers';

    public $defaultRoute = 'site';
    /**
     * {@inheritdoc}
     */
    public function init()
    {
        parent::init();

        Yii::$app->set('user', [
            'class' => 'yii\web\User',
            'identityClass' => 'app\modules\admin\models\Admin',
            'enableAutoLogin' => true,
            'loginUrl' => ['/admin/site/login'],
            'identityCookie' => ['name' => 'admin', 'httpOnly' => true],
            'idParam' => 'admin',  
        ]);
    }
}
